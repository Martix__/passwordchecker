# Passwordchecker

Op basis van volgende eisen:

- minstens 8 en niet meer dan 25 karakters hebben (een karakter is een letter, teken of spatie)
- minstens 1 hoofdletter hebben
- minstens 4 kleine letters hebben
- minstens 1 cijfer hebben
- minstens 1 teken hebben
- Toegestane tekens zijn: ! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { | } ~

command "pip install -r requirements.txt" voor het installeren van de nodige dependencies
