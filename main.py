#op basis van volgende eisen
#Aan welke eisen moet uw wachtwoord voldoen (voor ondernemers)
#Uw wachtwoord moet:

    #minstens 8 en niet meer dan 25 karakters hebben (een karakter is een letter, teken of spatie)
    #minstens 1 hoofdletter hebben
    #minstens 4 kleine letters hebben
    #minstens 1 cijfer hebben
    #minstens 1 teken hebben
    #Toegestane tekens zijn: ! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { | } ~

upteller = 0
lowteller = 0

tekenEis = "!#$%&'()*+,-./:;< = > ? @ [ \ ] ^ _ ` { | } ~"
illegaalEis = "± © § ⅓ ë Á ñ Ç µ"
lowletterEis = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
upletterEis = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

print("Welkom bij de wachtwoordchecker!")
passwordbox = input("Vul je wachtwoord hier in: ")

flag = 0

#Als het wachtwoord korter of gelijk is aan 7 (te kort) of langer of gelijk is aan 26 (te lang), dan ERROR
if len(passwordbox) <=7 or len(passwordbox) >=26:
    print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " moet minstens 8 en niet meer dan 25 karakters hebben (een karakter is een letter, teken of cijfer)" )
    flag = 1

#tellen van de hoofdletters, als uppercount 0 blijft (dus geen hoofdletters gevonden), dan ERROR
uppercount=0
for i in passwordbox:
      if(i.isupper()):
            uppercount=uppercount+1
if  uppercount == 0:
    print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " moet minstens 1 hoofdletter bevatten")
    flag = 1

#tellen van de kleine letters, als lowercount kleiner of gelijk aan 3 wordt (dus niet genoeg kleine letters), dan ERROR
lowercount=0
for i in passwordbox:
      if(i.islower()):
            lowercount=lowercount+1
if  lowercount <=3:
    print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " moet minstens 4 kleine letters bevatten")
    flag = 1


#Als er in passwordbox geen getal staat (dus wat er in MOET), dan ERROR
if not any(c.isdigit() for c in passwordbox):
        print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " moet minstens 1 cijfer bevatten")
        flag = 1

#Als er in passwordbox geen character staat van tekenEis (dus wat er in MOET), dan ERROR
if not any(c in tekenEis for c in passwordbox):
            print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " moet minstens 1 speciaal teken bevatten")
            flag = 1

#Als er in passwordbox een character staat van illegaalEis, dan ERROR
if any(c in illegaalEis for c in passwordbox):
                print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " bevat een niet-toegestane karakter")
                flag = 1

#if flag = 0 (Geen een keer de if in gegaan(=Geen een keer de fout in gegaan), dan is het wachtwoord dus ook goed.
if flag == 0:
    print("Het opgegeven wachtwoord " + "'" + passwordbox + "'" + " is sterk en voldoet aan de eisen!")